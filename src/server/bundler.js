const Bundler = require('parcel-bundler');

const options = {
	outDir: './dist', 
	outFile: 'index.html', 
	publicUrl: './',
	watch: true, 
	cache: false, 
	contentHash: false,
	minify: true, 
	target: 'browser', 
	logLevel: 3, 
	hmr: false, 
	sourceMaps: true, 
	detailedReport: true
};

module.exports = {
	createBundle(entry) {
		const bundler = new Bundler(entry, options);
		return bundler.bundle();
	}
}