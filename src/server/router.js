const path = require('path');

const cache = require('./cache.js');
const serve = require('./serve.js');

const CONFIG = require('../../config.json');

module.exports = {

	serveClient(request, reply) {
		const clientIndex = path.resolve(CONFIG.clientDirectory, './index.html');
		const file = serve.serveFile(clientIndex);
		if(!file.error) {
			reply.header('content-type', file.contentType);
			reply.send(file.stream);
		} else {
			reply.send(404);
		}
	},

	clientResources(request, reply) {
		const file = serve.serveFile("./dist/" + request.params.resource);
		if(!file.error) {
			reply.header('content-type', file.contentType);
			reply.send(file.stream);
		} else {
			reply.send(404);
		}
	},

	cache(request, reply) {
		cache.getCache().then(cacheFile => {
			reply.header('content-type', cacheFile.contentType);
			reply.code(200).send(cacheFile.stream);
		})
	},

	clearCache(request, reply) {
		cache.clearCache();
		reply.code(200).send("cleared");
	},

	upload(request, reply) {

	},

	files(request, reply) {
		
	}

}