const path = require('path');
const fs = require('fs');

const CONFIG = require('../../config.json');

function serveFile(filePath) {
	const out = {
		error: false,
		contentType: null,
		stream: null,
	};

	const file = path.resolve(filePath);
    const extname = path.extname(file);
	const validContentTypes = CONFIG.validContentTypes;

	let contentType = validContentTypes[extname];
	if(!contentType) {
		out.error = true;
	} else {
		out.contentType = contentType;
		out.stream = fs.createReadStream(file);
	}

	return out;
}

module.exports = {
	serveFile,
}