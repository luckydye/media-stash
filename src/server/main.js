const CONFIG = require('../../config.json');
const router = require('./router.js');
const fastify = require('fastify')({ 
	logger: false, 
});

// serve client
fastify.get('/', router.serveClient);
// client resources
fastify.get('/:resource', router.clientResources);
fastify.get('/files', router.files);
// file index
fastify.get('/index', router.cache);
fastify.get('/index/clear', router.clearCache);
// file upload
fastify.post('/upload', router.upload);

fastify.listen(CONFIG.port, (err, address) => {
	if (err) throw err;
	console.log(`Server listening on ${address}`);
});

// create bundle for development
// const entryFile = `${process.cwd()}/src/client/index.html`;
// require('./bundler.js').createBundle(entryFile);
