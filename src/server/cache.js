const path = require('path');
const fs = require('fs');
const serve = require('./serve.js');
const dirTree = require("directory-tree");

const CONFIG = require('../../config.json');

const cacheLoc = path.resolve(`${process.cwd()}/${CONFIG.cacheLocation}`);

function saveCache(data) {
	const josnData = JSON.stringify(data);
	fs.writeFile(cacheLoc, josnData, () => {
		console.log("cache written to file");
	});
}

function updateCache() {
	const tree = dirTree(CONFIG.publicDirectory);
	saveCache(tree);
	return tree;
}

function checkCache() {
	return new Promise((resolve, reject) => {
		fs.stat(cacheLoc, (err, stats) => {
			if(err) {
				updateCache();
				reject(err);
			} else {
				if(stats.size < 1) {
					updateCache();
				}
				resolve();
			}
		})
	})
}

async function getCache() {
	return checkCache().then(() => {
		return serve.serveFile(cacheLoc);
	})
}

function clearCache() {
	updateCache();
}

module.exports = {
	updateCache,
	getCache,
	clearCache,
}