import { Files } from "./Files.js";

window.addEventListener('DOMContentLoaded', init);

function init() {
	const filesContainer = document.querySelector('.fileIndex');

	Files.getIndex().then(data => {
		for(let file of data) {
			filesContainer.appendChild(createImagePreview(file));
		}
	})
}

function createImagePreview(file) {
	const node = document.createElement("div");
	node.className = "file";
	const name = file.name;
	const dlLink = `/files/${name}`;
	node.innerHTML = `
		<a href="${dlLink}" class="file-wrapper">
			<div class="file-preview">
				<img src="${dlLink}"/>
			</div>
			<div class="file-overlay">
				<span class="file-name">${name}</span>
			</div>
		</a>
	`;
	return node;
}