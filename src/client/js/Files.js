import { Config } from "./Config";

const filterFiles = {
	".png": "image/png",
	".jpg": "image/jpg"
}

export class Files {

	static getIndex() {
		const indexUrl = "/index";
		return fetch(indexUrl).then(res => {
			return res.json().then(data => {
				const maxFiles = 20;
				return data.children.slice(0, maxFiles).filter(file => {
					return filterFiles[file.extension];
				});
			})
		});
	}

}