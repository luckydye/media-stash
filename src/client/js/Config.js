import config from '../../../config.json';

export class Config {
	static get(str) {
		return config[str];
	}
}